package com.hcl.springcrudexample.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.springcrudexample.dto.Student;
import com.hcl.springcrudexample.repository.StudentRepository;

@Service
public class StudentService implements StudentService1 {

	@Autowired
	StudentRepository repository;

	public Student saveStudent(Student student) {
		return repository.save(student);

	}

	@Override
	public List<Student> getAllByStudent() {
		// TODO Auto-generated method stub
		return repository.findAll();
	}

	@Override
	public Student getById(int id) {
		// TODO Auto-generated method stub
		return repository.findById(id).get();
	}

	@Override
	public boolean deleteById(int id) {
		Student student = repository.getById(id);
		if (student != null) {
			repository.deleteById(id);
			return true;
		}
		else
			return false;
	}

	

	@Override
	public boolean updateById(int id, Student student) {
		Student student1=repository.getOne(id);
		if(student1!=null) {
			repository.save(student);
		return true;
		}
		else
		 return false;
	}

}
