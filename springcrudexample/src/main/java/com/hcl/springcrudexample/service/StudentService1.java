package com.hcl.springcrudexample.service;

import java.util.List;

import com.hcl.springcrudexample.dto.Student;

public interface StudentService1 {
	public Student saveStudent(Student student);

	public List<Student> getAllByStudent();

	public Student getById(int id);

	public boolean deleteById(int id);
	
	public boolean  updateById(int id,Student student);

}
