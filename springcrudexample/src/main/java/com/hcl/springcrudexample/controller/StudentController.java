package com.hcl.springcrudexample.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.springcrudexample.dto.Student;
import com.hcl.springcrudexample.service.StudentService;

@RestController

public class StudentController {
	
	@Autowired
	StudentService studentService;
	
	@PostMapping(value = "/savestudent")
	public Student saveStudent(@RequestBody Student student) {
		return studentService.saveStudent(student);
	}
	
	@GetMapping(value="/getallstudents")
	public List<Student> getAllByStudent() 
	{
		return studentService.getAllByStudent();
	}
	
	@GetMapping(value="/student/{id}")
	public Student getById(@PathVariable("id") int id) {
		return studentService.getById(id);
	}
	
	@DeleteMapping(value="/student/{id}")
	public boolean deleteById(@PathVariable int  id) {
		return studentService.deleteById(id);
		}
	
	@PutMapping(value="/student/{id}")
	public boolean updateById(@PathVariable int id, @RequestBody Student student) {
		return  studentService.updateById(id, student);
	}
}

