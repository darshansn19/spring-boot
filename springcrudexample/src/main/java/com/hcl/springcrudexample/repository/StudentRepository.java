package com.hcl.springcrudexample.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.stereotype.Repository;

import com.hcl.springcrudexample.dto.Student;
@Repository
public interface StudentRepository extends JpaRepository<Student, Integer>{

}
