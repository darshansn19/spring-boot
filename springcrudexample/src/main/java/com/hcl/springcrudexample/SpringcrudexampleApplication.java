package com.hcl.springcrudexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringcrudexampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringcrudexampleApplication.class, args);
	}

}
