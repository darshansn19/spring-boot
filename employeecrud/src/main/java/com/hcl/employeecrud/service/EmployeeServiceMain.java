package com.hcl.employeecrud.service;

import java.sql.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;

import com.hcl.employeecrud.dto.Employe;
import com.hcl.employeecrud.repository.EmployeeRepository;

@Service
public class EmployeeServiceMain implements EmployeService {

	@Autowired
	EmployeeRepository employeeRepository;

	@Override
	public Employe saveEmployee(Employe employe) {
		return employeeRepository.save(employe);
	}

	@Override
	public List<Employe> getAllEmployes() {
		// TODO Auto-generated method stub
		return employeeRepository.findAll();
	}

	@Override
	public Employe getEmployeById(int id) {
		return employeeRepository.findById(id).get();

	}

	@Override
	public boolean updateEmployee(int id, Employe employe) {
		Employe employe1 = employeeRepository.getById(id);
		if (employe1 != null) {
			employeeRepository.save(employe);
			return true;
		} else
			return false;
	}

	@Override
	public boolean deleteEmployee(int id) {
		Employe employe2 = employeeRepository.getOne(id);
		if (employe2 != null) {
			employeeRepository.deleteById(id);
			return true;
		} else
			return false;
	}

	@Override
	public List<Employe> getempployeByFname(String fname) {

		return employeeRepository.findByFname(fname);
	}

	@Override
	public List<Employe> getempployeByLname(String lname) {
		// TODO Auto-generated method stub
		return employeeRepository.findByLname(lname);
	}

	@Override
	public List<Employe> getEmployeeByFnameAndLname(String fname, String lname) { 
		// TODO Auto-generated method stub
				return employeeRepository.findByFnameAndLname(fname, lname);
	}

	@Override
	public List<Employe> getEmployeeByFnameOrfindByLname(String fname, String lname) {
		return employeeRepository.findByFnameOrLname(fname, lname);
	}

	@Override
	public List<Employe> getEmployeByAgeGreaterThan(int id) {
		return employeeRepository.findByAgeGreaterThan(id);
	}

	@Override
	public List<Employe> getEmployeByFnameLike(String fname) {
		// TODO Auto-generated method stub
		return employeeRepository.findByFnameLike(fname);
	}

	public List<Employe> getEmployeByfnamecontains(String fname) {
		return employeeRepository.findByFnameContains(fname);
		
		
	}

	@Override
	public List<Employe> getEmployeeByfnameIs(String fname) {
		return employeeRepository.findByFnameIs(fname);
	}
	@Override
	public List<Employe> getEmployeeByFnameEquals(String fname) {
		
		return employeeRepository.findByFnameEquals(fname);
	}

	@Override
	public List<Employe> getEmployesBetweenStartdate(Date startdate, Date lastdate) {
		
		return employeeRepository.findByJoiningDateBetween(startdate,lastdate);
	}

	@Override
	public List<Employe> getEmployeeByemailEndingWith(String email) {
		// TODO Auto-generated method stub
		return employeeRepository.findByEmailEndingWith(email);
	}


	@Override
	public List<Employe> getEmployeeJoijingAfter(Date startdate) {
		// TODO Auto-generated method stub
		return employeeRepository.findByJoiningDateAfter(startdate);
	}

	@Override
	public List<Employe> getEmployeByAgeOrderbyfname(int age) {
		// TODO Auto-generated method stub
		return employeeRepository.findByAgeOrderByFname(age);
	}

	@Override
	public List<Employe> getEmployesByLnameByIrnoringcase(String lname) {
		// TODO Auto-generated method stub
		return employeeRepository.findByLnameIgnoreCase(lname);
	}

	@Override
	public List<Employe> getAllEmployeeBypagination(int pageNumber, int pageSize) {
		Pageable pageable=PageRequest.of(pageNumber, pageSize,Sort.by(Direction.ASC, "fname"));
		return employeeRepository.findAll(pageable).getContent();

	}

	@Override
	public List<Employe> getAllEmployeAgeIn(List<Integer> age) {
		
		return employeeRepository.findByAgeIn(age);
	}

	

	
	

}
