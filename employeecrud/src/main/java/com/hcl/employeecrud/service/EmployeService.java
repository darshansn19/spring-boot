package com.hcl.employeecrud.service;

import java.sql.Date;
import java.util.List;

import com.hcl.employeecrud.dto.Employe;

public interface EmployeService {
	
	public Employe saveEmployee(Employe employe);
	public List<Employe> getAllEmployes();
	public Employe getEmployeById(int id);
	public boolean updateEmployee(int id,Employe employe);
	public boolean deleteEmployee(int id);
	 
	List<Employe> getempployeByFname(String fname);
	List<Employe> getempployeByLname(String  lname);
	
	 List<Employe> getEmployeeByFnameAndLname(String fname,String lname);
	 List<Employe> getEmployeeByFnameOrfindByLname(String fname,String lname);
	  
	 List<Employe> getEmployeByAgeGreaterThan(int id);
	  List<Employe> getEmployeByFnameLike(String fname);
 List<Employe> getEmployeByfnamecontains(String fname);
	 
	List<Employe> getEmployeeByfnameIs(String fname);
	List<Employe> getEmployeeByFnameEquals(String fname);

	List<Employe> getEmployesBetweenStartdate(Date startdate, Date lastdate);
	
	List<Employe> getEmployeeByemailEndingWith(String email);
	
	List<Employe> getEmployeeJoijingAfter(Date startdate);
	List<Employe> getEmployeByAgeOrderbyfname(int age);
	List<Employe> getEmployesByLnameByIrnoringcase(String lname);
	List<Employe> getAllEmployeeBypagination(int pageNumber, int pageSize);
	
	List<Employe> getAllEmployeAgeIn(List<Integer> age);
	  
	  
	  
	  
	  
}

