package com.hcl.employeecrud.repository;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcl.employeecrud.dto.Employe;

@Repository
public interface EmployeeRepository extends JpaRepository<Employe, Integer> {

	List<Employe> findByFname(String fname);

	List<Employe> findByLname(String lname);

	List<Employe> findByFnameAndLname(String fname, String lname);

	List<Employe> findByFnameOrLname(String fname, String lname);

	List<Employe> findByAgeGreaterThan(int id);

	List<Employe> findByFnameLike(String fname);

	List<Employe> findByFnameContains(String fname);

	List<Employe> findByFnameIs(String fname);

	List<Employe> findByFnameEquals(String fname);

	List<Employe> findByJoiningDateBetween(Date startdate, Date lastdate);

	List<Employe> findByEmailEndingWith(String email);

	List<Employe> findByJoiningDateAfter(Date startdate);

	List<Employe> findByAgeOrderByFname(int age);

	List<Employe> findByLnameIgnoreCase(String lname);

	List<Employe> findByAgeIn(List<Integer> ageLis);
}
