package com.hcl.employeecrud.controller;


import java.sql.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.employeecrud.dto.Employe;
import com.hcl.employeecrud.service.EmployeeServiceMain;

@RestController
public class EmployeController {
	@Autowired
	EmployeeServiceMain employeeServiceMain;

	@PostMapping(value = "/saveemploye")
	public Employe saveEmployee(@RequestBody Employe employe) {
		return employeeServiceMain.saveEmployee(employe);
	}

	@GetMapping(value = "/getall")
	public List<Employe> getAllEmployes() {
		return employeeServiceMain.getAllEmployes();
	}

	@GetMapping(value = "/getone/{id}")
	public Employe getEmployeById(@PathVariable("id") int id) {
		return employeeServiceMain.getEmployeById(id);
	}

	@PutMapping(value = "/update/{id}")
	public boolean updateEmployee(@PathVariable("id") int id, @RequestBody Employe employe) {
		return employeeServiceMain.updateEmployee(id, employe);
	}

	@DeleteMapping(value = "/delete/{id}")
	public boolean deleteEmployee(@PathVariable("id") int id) {
		return employeeServiceMain.deleteEmployee(id);
	}

	@GetMapping(value = "/byfname/{fname}")
	public List<Employe> getempployeByFname(@PathVariable("fname") String fname) {
		return employeeServiceMain.getempployeByFname(fname);
	}

	@GetMapping(value = "/bylname/{lname}")
	public List<Employe> getempployeByLname(@PathVariable("lname") String lname) {
		return employeeServiceMain.getempployeByLname(lname);

	}

	@GetMapping(value = "/byandfl/{fname}/{lname}") 
	public List<Employe> getEmployeeByFnameAndLname(@PathVariable("fname") String fname, @PathVariable("lname")String lname)
	{ 
		return employeeServiceMain.getEmployeeByFnameAndLname(fname, lname);
	 
	}

	@GetMapping(value = "/byorfl/{fname}/{lname}") 
	public List<Employe> getEmployeeByFnameOrfindByLname(@PathVariable("fname") String fname, @PathVariable("lname")String lname) {
		return employeeServiceMain.getEmployeeByFnameOrfindByLname(fname, lname);
	  
	  }
	
	@GetMapping(value = "/getempbyagegreaterthan/{id}")
	public List<Employe> getEmployeByAgeGreaterThan(@PathVariable("id")   int id){
		return employeeServiceMain.getEmployeByAgeGreaterThan(id);
	}
	
	@GetMapping(value = ("/getempbylike/{fname}"))
	public List<Employe> getEmployeByFnameLike(@PathVariable("fname") String fname){
		return  employeeServiceMain.getEmployeByFnameLike(fname);
	}
	 
	@GetMapping(value ="/getEmployeByfnamecontains")
	public List<Employe> getEmployeByfnamecontains(@RequestParam String fname)
	{
		return employeeServiceMain.getEmployeByfnamecontains(fname);
	}
	
	@GetMapping(value="/getEmployeeByfnameIs")
	public List<Employe> getEmployeeByfnameIs(@RequestParam String fname){
		return employeeServiceMain.getEmployeeByfnameIs(fname);
		
	}
	
	@GetMapping(value="/getEmployeeByFnameEquals")
	public List<Employe> getEmployeeByFnameEquals(@RequestParam String fname)
	{
		return employeeServiceMain.getEmployeeByFnameEquals(fname);
		
	}
	@GetMapping(value = "/getEmployesBetweenStartdate")
	public List<Employe> getEmployesBetweenStartdate(@RequestParam Date startdate,@RequestParam Date lastdate)
	{
		return employeeServiceMain.getEmployesBetweenStartdate(startdate,lastdate);
		 
	}
	
	@GetMapping(value = "/getEmployeeJoijingAfter")
	public List<Employe> getEmployeeJoijingAfter(@RequestParam Date startdate){
		return employeeServiceMain.getEmployeeJoijingAfter(startdate);
		
	}
	
	@GetMapping(value = "/getEmployeeByemailEndingWith")
	public List<Employe> getEmployeeByemailEndingWith(@RequestParam String email)
	{
		return employeeServiceMain.getEmployeeByemailEndingWith(email);
		
	}
	
	@GetMapping(value = "/getEmployeByAgeOrderbyfname")
	public List<Employe> getEmployeByAgeOrderbyfname(int age)
	{
		return employeeServiceMain.getEmployeByAgeOrderbyfname(age);
	}
	
	@GetMapping(value = "/getEmployesByLnameByIrnoringcase")
	public List<Employe> getEmployesByLnameByIrnoringcase(String lname)
	{
		return employeeServiceMain.getEmployesByLnameByIrnoringcase(lname);
		
	}
	
	@GetMapping(value="/getAllEmployeeBypagination")
	public List<Employe> getAllEmployeeBypagination(int pageNumber,int  pageSize)
	{
		return employeeServiceMain.getAllEmployeeBypagination(pageNumber,pageSize);
	}
	
	@GetMapping("/getAllStudentAgeIn")
	 public List<Employe> getEmployeAgeIn(@RequestParam List<Integer> age){
	     
	        return employeeServiceMain.getAllEmployeAgeIn(age);
	}
}
