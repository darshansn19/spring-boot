package com.alvas.boot.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.alvas.boot.dto.Customer;

public interface CustomerRepository  extends JpaRepository<Customer, Integer>{

}
