package com.alvas.boot.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.alvas.boot.dao.CustomerDao;
import com.alvas.boot.dao.ResponseStructure;
import com.alvas.boot.dto.Customer;
import com.alvas.boot.exception.NoIdFoundException;

@Service
public class CustomerService {
@Autowired
	CustomerDao customerDao;
	
	public Customer saveCustomer(Customer customer) {
		return customerDao.saveCustomer(customer);
	}

	public List<Customer> getAllCustmer() {
		return customerDao.getAllCustmer();
	}

	public ResponseStructure<Customer> getCustomerById(int id) {
		
			Customer customer=customerDao.getCustomerById(id);
			ResponseStructure<Customer> responseStructure=new ResponseStructure<>();
			if(customer!=null)
			{
				responseStructure.setStatusCode(HttpStatus.FOUND.value());
				responseStructure.setMessage("customer found");
				responseStructure.setData(customer);
				return responseStructure;
			}
			else{
				throw new NoIdFoundException("no customer with given id");
			}
			}
	public boolean deleteById(int id) {
		
			return customerDao.deleteById(id);
	}

	public Customer updateById(int id,Customer customer) {
		
			return customerDao.updateById(id, customer);

	}
}
