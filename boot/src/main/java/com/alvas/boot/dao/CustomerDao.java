package com.alvas.boot.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.alvas.boot.dto.Customer;
import com.alvas.boot.repository.CustomerRepository;
@Repository
public class CustomerDao {

	@Autowired
	CustomerRepository customerRepository;

	public Customer saveCustomer(Customer customer) {
		return customerRepository.save(customer);
	}

	public List<Customer> getAllCustmer() {
		return customerRepository.findAll();
	}

	public Customer getCustomerById(int id) {
		Optional<Customer> optional = customerRepository.findById(id);
		if (optional.isPresent()) {
			return optional.get();
		} else
			return null;
	}

	public boolean deleteById(int id) {
		Customer customer = getCustomerById(id);
		if (customer != null) {
			customerRepository.delete(customer);
			return true;
		}

		else
			return false;
	}

	public Customer updateById(int id,Customer customer) {
		Customer customer2 = getCustomerById(id);
		if(customer2!=null)
		{
			return customerRepository.save(customer);
		}
		else
			return null;

	}
}
