package com.alvas.boot.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alvas.boot.dao.ResponseStructure;
import com.alvas.boot.dto.Customer;
import com.alvas.boot.service.CustomerService;

@RestController
public class CustomerController {

	@Autowired
	CustomerService customerService;

	@PostMapping("/customers")
	public Customer saveCustomer(@RequestBody Customer customer) {
		return customerService.saveCustomer(customer);

	}

	@GetMapping("/customers")
	public List<Customer> getAllCustomer()
	{
		return customerService.getAllCustmer();
		
	}
	
	@GetMapping("/customers/{id}")
	public  ResponseStructure<Customer>  getCustomerById(@PathVariable int id){
	return customerService.getCustomerById(id);
	}
	
	@DeleteMapping("/customers")
	public boolean deleteCustomer(@RequestParam int id)
	{
		return customerService.deleteById(id);
	}
	
	@PutMapping("/customers/{id}")
	public Customer updateCustomer(@PathVariable int id, @RequestBody Customer customer)
	{
		return customerService.updateById(id, customer);
	}
	
}
