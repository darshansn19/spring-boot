package com.alvas.boot.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.alvas.boot.dao.ResponseStructure;

@ControllerAdvice
public class ApplicationHandler {
	@ExceptionHandler(value=NoIdFoundException.class)
	public ResponseEntity<ResponseStructure<String>> noIdFoundException(NoIdFoundException e){
		ResponseStructure<String> responseStructure=new ResponseStructure<String>();
		responseStructure.setData("no id found");
		responseStructure.setMessage(e.getMessage());
		responseStructure.setStatusCode(HttpStatus.NOT_FOUND.value());
		
		return new ResponseEntity<ResponseStructure<String>>(responseStructure, HttpStatus.NOT_FOUND);
		
	}

}
