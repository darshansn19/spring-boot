package com.alvas.boot.exception;

public class NoIdFoundException extends RuntimeException {
	private String message;

	public NoIdFoundException(String message) {
		super();
		this.message = message;
	}
	public String getMessage()
	{
		return message;
	}
}
